### **Description**
Android app implemented to break md5 hashes using brute force attack implemented in C++

---
### **Technology**
Android, NDK

---
### **Year**
2015

---
### **Screenshot**

![](/README/Screenshot_2015-05-06-13-56-40.png)
![](/README/Screenshot_2015-05-06-13-58-19.png)