#include <android/log.h>
#include <stdio.h>
#include <stdlib.h>
#include <jni.h>
#include <string.h>
#include <md5.h>

#define  LOG_TAG    "LibBruteForce"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

extern "C" {
JNIEXPORT jstring JNICALL
Java_com_example_iri_bruteforceandroid_MainActivity_attack( JNIEnv* env, jobject  obj, jstring array);
JNIEXPORT jint JNICALL
Java_com_example_iri_bruteforceandroid_MainActivity_hashes( JNIEnv* env, jobject  obj);
}


//konstanty
const int max_characters = 9;
int maxLevelNow = 0;
char field[max_characters];
char hash[16 * 2 + 1] = "11649b4394d09e4aba132ad49bd1e7db";
char pomHash[16 * 2 + 1];
char currentPasswd[max_characters];

//jen mala pismena
int low = 97;
int high = 122 + 1;
int count_generated_hashes = 0;


//32 = SPACE, 48 = 0; 65 = A; 97 = a; 126 = ~
/*
int low = 32;
int high = 127;
*/

void str2md5() {

	count_generated_hashes++;
	md5_state_t state;
	md5_byte_t digest[16];
	int di;

	md5_init(&state);
	md5_append(&state, (const md5_byte_t *)currentPasswd, strlen(currentPasswd));
	md5_finish(&state, digest);
	for (di = 0; di < 16; ++di)
		sprintf(pomHash + di * 2, "%02x", digest[di]);
}

bool compare(char *hash, char *pomHash){
	for (int i = 0; i < 33; i++){
		if (hash[i] != pomHash[i]){
			return false;
		}
	}
	return true;
}

bool bruteforce(int level = 0){
	while (true)
	{
		for (int x = low; x < high; x++)
		{
			if (level == max_characters)
			{
				return false;
			}
			field[level] = (char)x;
			if (level != 0)
			{
				if (bruteforce(level - 1))
				{
					return true;
				}
			}

			for (int i = 0; i < maxLevelNow + 1; i++){
				currentPasswd[i] = field[i];
			}

			str2md5();
			if (compare(hash, pomHash))
			{
				//konec
				//printf("SHODA\n");
				LOGI("SHODA\n");
				for (int i = 0; i <= maxLevelNow; i++)
				{
					//printf("%c", field[i]);
					LOGI("%c", field[i]);
				}
				//printf("\n");
				//LOGI("\n");
				return true;
			}
			else
			{
				if (level == maxLevelNow)
				{
					if (level != 0 && x == low)
					{
						if (bruteforce(level - 1))
						{
							return true;
						}
					}
				}
			}
		}
		if (level == maxLevelNow)
		{
			//printf("Porovnana vsechna slova s delkou %d \n", (level + 1));
			LOGI("Porovnana vsechna slova s delkou %d \n", (level + 1));
			maxLevelNow = ++level;
		}
		else
		{
			return false;
		}
	}
}

JNIEXPORT jint JNICALL
Java_com_example_iri_bruteforceandroid_MainActivity_hashes( JNIEnv* env, jobject obj)
{
	return count_generated_hashes;
}

JNIEXPORT jstring JNICALL
Java_com_example_iri_bruteforceandroid_MainActivity_attack( JNIEnv* env, jobject  obj, jstring array)
{
    //strncpy(hash, "4124bc0a9335c27f086f24ba207a4912", 33);
    //inicializace - je potreba pro kazde dalsi (neprvni) spusteni vynulovat pamet....
    maxLevelNow = 0;
    count_generated_hashes = 0;
	memset ( field, 0, max_characters );
	memset ( currentPasswd, 0, max_characters );
	memset ( hash, 0, 33 );
	memset ( pomHash, 0, 33 );

	const char* pom = env->GetStringUTFChars(array, 0);
	for(int i = 0; i < 33; i++)
	{
		hash[i] = pom[i];
	}
	//je potreba promennou uvolnit
	(env)->ReleaseStringUTFChars(array , pom);

	jstring jstrBuf = env->NewStringUTF("");
	if(bruteforce(0)){
		//prepis puvodniho pole
		jstrBuf = env->NewStringUTF(field);
		/*
		for (int i = 0; i <= maxLevelNow; i++)
		{
			array[i] = field[i]
		}
		for (int i = maxLevelNow+1; i < 33; i++)
		{
			array[i] = '';
		}
		*/

	}
	LOGI("Bylo projito: %d hashu.\n", count_generated_hashes);
	return jstrBuf;

}

