LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_LDLIBS := -llog -landroid -lstdc++ -ldl -ljnigraphics

LOCAL_MODULE    := bruteForce
LOCAL_SRC_FILES := bitmap.cpp

include $(BUILD_SHARED_LIBRARY)
