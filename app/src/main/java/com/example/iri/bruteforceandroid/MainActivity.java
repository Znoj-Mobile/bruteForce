package com.example.iri.bruteforceandroid;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    static {
        System.loadLibrary("bruteForce");
    }

    private static native String attack(String s);
    private static native int hashes();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = getTitle();

        // add the fragment
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {

        super.onPause();

        getSupportFragmentManager().findFragmentById(R.id.navigation_drawer)
                .setRetainInstance(true);
    }

    @Override
    protected void onResume() {

        super.onResume();

        getSupportFragmentManager().findFragmentById(R.id.navigation_drawer)
                .getRetainInstance();

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.brute_force);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            //getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";


        final Handler mHandler = new Handler();


        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            final TextView password = (TextView) rootView.findViewById(R.id.password_text_view);
            final TextView md5_text_view = (TextView) rootView.findViewById(R.id.md5_text_view);
            final TextView md5_result = (TextView) rootView.findViewById(R.id.md5_result);
            final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);

            Button generate_MD5 = (Button) rootView.findViewById(R.id.generate_MD5);
            generate_MD5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    progressBar.setVisibility(View.VISIBLE);
                    final String passwd_text = password.getText().toString();
                    if(passwd_text.equals("")){
                        final AlertDialogWrapper.Builder builder = new AlertDialogWrapper.Builder(getActivity());
                        builder.setTitle(getString(R.string.password_cant_be_null));
                        builder.setCancelable(false);
                        builder.setMessage(getString(R.string.password_cant_be_null_please_set_password));
                        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog2, int which) {
                                dialog2.dismiss();
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                        Dialog d = builder.create();
                        d.show();
                    }
                    else{
                        SnackbarManager.show(
                                Snackbar.with(getActivity())
                                        .text(getString(R.string.generate_md5_success)));
                        new Thread(new Runnable() {
                            public void run() {
                                final String md5_result = md5(passwd_text);

                                // Update the progress bar
                                mHandler.post(new Runnable() {
                                    public void run() {
                                        md5_text_view.setText(md5_result);
                                        progressBar.setVisibility(View.GONE);
                                    }
                                });
                            }
                        }).start();
                    }
                    //schovani klavesnice
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(password.getWindowToken(), 0);
                }
            });
            Button start_attack = (Button) rootView.findViewById(R.id.button2);
            start_attack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    progressBar.setVisibility(View.VISIBLE);

                    final String md5_hash_text = md5_text_view.getText().toString();

                    if(md5_hash_text.equals("")){
                        final AlertDialogWrapper.Builder builder = new AlertDialogWrapper.Builder(getActivity());
                        builder.setTitle(getString(R.string.md5_cant_be_null));
                        builder.setCancelable(false);
                        builder.setMessage(getString(R.string.md5_cant_be_null_please_set_md5));
                        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog2, int which) {
                                dialog2.dismiss();
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                        Dialog d = builder.create();
                        d.show();
                    }
                    else{
                        Log.d("NDKActivity", "Starting ...");
                        new Thread(new Runnable() {
                            public void run() {
                                long startTime = System.currentTimeMillis();
                                final String md5_result_text = attack(md5_hash_text);
                                final long difference = System.currentTimeMillis() - startTime;
                                final int md5_number_of_hashes = hashes();

                                // Update the progress bar
                                mHandler.post(new Runnable() {
                                    public void run() {
                                        md5_result.setText(md5_result_text + " " + "\n\nporovnano " + md5_number_of_hashes + " hashu za " + difference + "ms.\n\n ("+ md5_number_of_hashes/(difference/1000) + " za vterinu)");
                                        progressBar.setVisibility(View.GONE);
                                    }
                                });
                            }
                        }).start();
                        Log.d("NDActivity", "Finished");
                    }

                }
            });
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }

        //metoda pro generovani md5
        private static String md5(String s) {
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] messageDigest = md.digest(s.getBytes());
                BigInteger number = new BigInteger(1, messageDigest);
                String hashtext = number.toString(16);
                // Now we need to zero pad it if you actually want the full 32 chars.
                while (hashtext.length() < 32) {
                    hashtext = "0" + hashtext;
                }
                return hashtext;
            }
            catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }

        }
    }

}
